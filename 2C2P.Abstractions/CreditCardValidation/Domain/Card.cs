﻿namespace _2C2P.Business.CreditCardValidation.Domain
{
    /// <summary>
    /// Represents a credit card
    /// </summary>
    public class Card
    {
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
    }
}
