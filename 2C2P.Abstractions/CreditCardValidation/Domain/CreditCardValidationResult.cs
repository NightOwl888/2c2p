﻿namespace _2C2P.Business.CreditCardValidation.Domain
{
    /// <summary>
    /// The validity status of a credit card validation operation
    /// </summary>
    public enum CreditCardValidationResult
    {
        Valid,
        Invalid,
        DoesNotExist
    }
}
