﻿namespace _2C2P.Business.CreditCardValidation.Domain
{
    /// <summary>
    /// The result of a credit card validation operation
    /// </summary>
    public class ValidateResult
    {
        public CreditCardValidationResult Result { get; set; }
        public string CardType { get; set; }
    }
}
