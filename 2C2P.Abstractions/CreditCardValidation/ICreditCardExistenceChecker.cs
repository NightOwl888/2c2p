﻿namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// Contract for service that checks existence of the credit card in an external source (database).
    /// </summary>
    public interface ICreditCardExistenceChecker
    {
        bool CreditCardExists(string cardNumber);
    }
}
