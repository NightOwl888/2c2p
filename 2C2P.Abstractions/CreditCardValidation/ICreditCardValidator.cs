﻿namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// Contract for a service that validates a credit card number.
    /// </summary>
    public interface ICreditCardValidator
    {
        /// <summary>
        /// Validates a credit card number.
        /// </summary>
        /// <param name="card">A card to validate (number and expiry date).</param>
        /// <returns>A <see cref="Domain.ValidateResult"/>, which both identifies the credit card and specifies whether it is valid.</returns>
        Domain.ValidateResult Validate(Domain.Card card);
    }
}
