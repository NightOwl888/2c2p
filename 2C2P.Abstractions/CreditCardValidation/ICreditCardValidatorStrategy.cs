﻿using System;

namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// Contact for credit card type strategies, which encapsulate
    /// both the identification and validation logic for a single credit card type.
    /// </summary>
    public interface ICreditCardValidatorStrategy
    {
        string CardType { get; }
        bool IsValid(string cardNumber, DateTime expiryDate);
        bool AppliesTo(string cardNumber);
    }
}
