﻿using System;

namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// Contract for a parser of the expiry date
    /// </summary>
    public interface IExpiryDateParser
    {
        bool TryParse(string expiryDate, out DateTime date);
    }
}
