﻿using System;

namespace _2C2P.Business
{
    /// <summary>
    /// Contract for a service that provides the current time. This is an abstraction for testing.
    /// </summary>
    public interface ISystemClock
    {
        DateTime GetCurrentTime();
    }
}
