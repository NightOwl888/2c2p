﻿using _2C2P.Business.CreditCardValidation;
using _2C2P.Business.CreditCardValidation.Domain;
using _2C2P.Data.Seeding;
using Swashbuckle.Examples;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace _2C2P.Api.Controllers
{
    [RoutePrefix("api/v1/creditcard")]
    public class CreditCardController : ApiController
    {
        private readonly ICreditCardValidator creditCardValidator;

        public CreditCardController(ICreditCardValidator creditCardValidator)
        {
            this.creditCardValidator = creditCardValidator
                ?? throw new ArgumentNullException(nameof(creditCardValidator));
        }

        /// <summary>
        /// Validates a credit card and expiry date.
        /// </summary>
        /// <param name="card">Instance of <see cref="Card"/>.</param>
        /// <returns>Returns 200.</returns>
        [HttpPost, Route("validate"), ResponseType(typeof(ValidateResult))]
        [SwaggerRequestExample(typeof(Card), typeof(ValidationRequestExample))]
        [SwaggerResponseExample(System.Net.HttpStatusCode.OK, typeof(ValidationResponseExample))]
        public IHttpActionResult Post(Card card)
        {
            var result = creditCardValidator.Validate(card);

            return Ok(result);
        }

        public class ValidationRequestExample : IExamplesProvider
        {
            private static List<Card> pool = new List<Card>();
            private static Random random = new Random();

            static ValidationRequestExample()
            {
                var temp = SampleCreditCards.GetCreditCards();
                for (int i = 0; i < 8; i++)
                {
                    int index = random.Next(0, temp.Length);
                    string cardNumber = temp[index].CardNumber;
                    string expiryDate = GetExpiryDate(cardNumber);
                    pool.Add(new Card { CardNumber = cardNumber, ExpiryDate = expiryDate });
                }
            }

            private static string GetExpiryDate(string cardNumber)
            {
                if (cardNumber.StartsWith("4"))
                    return "072024";
                else if (cardNumber.StartsWith("5"))
                    return "092029";
                else
                    return "112034";
            }

            public object GetExamples()
            {
                return pool[random.Next(0, 8)];
            }
        }

        public class ValidationResponseExample : IExamplesProvider
        {
            public object GetExamples()
            {
                return new ValidateResult
                {
                    CardType = "Amex",
                    Result = CreditCardValidationResult.Valid
                };
            }
        }
    }
}
