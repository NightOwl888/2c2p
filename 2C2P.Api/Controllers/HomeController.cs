﻿using System;
using System.Web.Http;
using System.Web.Http.Description;

namespace _2C2P.Api.Controllers
{
    /// <summary>
    /// Stub controller that is used to redirect to the swagger page
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : ApiController
    {
        [Route("")]
        public IHttpActionResult Get()
        {
            var builder = new UriBuilder(this.Request.RequestUri);
            builder.Query = "";
            builder.Path = "swagger";

            return Redirect(builder.ToString());
        }
    }
}
