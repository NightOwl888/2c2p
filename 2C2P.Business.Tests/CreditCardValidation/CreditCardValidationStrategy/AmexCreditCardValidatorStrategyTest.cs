﻿using NUnit.Framework;
using System;

namespace _2C2P.Business.CreditCardValidation
{
    [TestFixture]
    public class AmexCreditCardValidatorStrategyTest
    {
        private ICreditCardValidatorStrategy target;
        private DateTime validExpiryDate;

        [SetUp]
        public void SetUp()
        {
            target = new AmexCreditCardValidatorStrategy();
            validExpiryDate = new DateTime(2020, 11, 1);
        }

        [TearDown]
        public void TearDown()
        {
            target = null;
            validExpiryDate = new DateTime();
        }

        [Test]
        public void TestCardTypeReturnsAmex()
        {
            Assert.AreEqual("Amex", target.CardType);
        }

        [Test]
        public void TestAppliesToBeginsWith3()
        {
            Assert.IsTrue(target.AppliesTo("311122233344455"));
        }

        [Test]
        public void TestAppliesToBeginsWith3AndLengthTooShort()
        {
            // IsValid tests for the correct length. Here we are
            // only concerned that it is using the correct validator.
            Assert.IsTrue(target.AppliesTo("31112223334445"));
            Assert.IsTrue(target.AppliesTo("3111222"));
            Assert.IsTrue(target.AppliesTo("3"));
        }

        [Test]
        public void TestAppliesToBeginsWith3AndLengthTooLong()
        {
            Assert.IsFalse(target.AppliesTo("3111222333444555"));
            Assert.IsFalse(target.AppliesTo("3111222333444555666"));
            Assert.IsFalse(target.AppliesTo("3111222333444555666777"));
        }

        [Test]
        public void TestAppliesToNull()
        {
            Assert.IsFalse(target.AppliesTo(null));
        }

        [Test]
        public void TestAppliesToEmptyString()
        {
            Assert.IsFalse(target.AppliesTo(string.Empty));
        }

        [Test]
        public void TestIsValidCorrectLength()
        {
            Assert.IsTrue(target.IsValid("311122233344455", validExpiryDate));
        }

        [Test]
        public void TestIsValidIncorrectLength()
        {
            Assert.IsFalse(target.IsValid("31112223334445", validExpiryDate));
            Assert.IsFalse(target.IsValid("3", validExpiryDate));
            Assert.IsFalse(target.IsValid("3111222333444555", validExpiryDate));
            Assert.IsFalse(target.IsValid("31112223334445556", validExpiryDate));
        }

        [Test]
        public void TestIsValidNull()
        {
            Assert.IsFalse(target.IsValid(null, validExpiryDate));
        }

        [Test]
        public void TestIsValidEmptyString()
        {
            Assert.IsFalse(target.IsValid(string.Empty, validExpiryDate));
        }
    }
}
