﻿using NUnit.Framework;
using System;

namespace _2C2P.Business.CreditCardValidation
{
    [TestFixture]
    public class JCBCreditCardValidatorStrategyTest
    {
        private ICreditCardValidatorStrategy target;
        private DateTime validExpiryDate;

        [SetUp]
        public void SetUp()
        {
            target = new JCBCreditCardValidatorStrategy();
            validExpiryDate = new DateTime(2020, 11, 1);
        }

        [TearDown]
        public void TearDown()
        {
            target = null;
            validExpiryDate = new DateTime(2020, 11, 1);
        }

        [Test]
        public void TestCardTypeReturnsJCB()
        {
            Assert.AreEqual("JCB", target.CardType);
        }

        [Test]
        public void TestAppliesToBeginsWith3()
        {
            Assert.IsTrue(target.AppliesTo("3111222333444555"));
        }

        [Test]
        public void TestAppliesToBeginsWith3AndLengthTooShort()
        {
            Assert.IsFalse(target.AppliesTo("311122233344455"));
            Assert.IsFalse(target.AppliesTo("3111222"));
            Assert.IsFalse(target.AppliesTo("3"));
        }

        [Test]
        public void TestAppliesToBeginsWith3AndLengthTooLong()
        {
            // IsValid tests for the correct length. Here we are
            // only concerned that it is using the correct validator.
            Assert.IsTrue(target.AppliesTo("31112223334445556"));
            Assert.IsTrue(target.AppliesTo("3111222333444555666"));
            Assert.IsTrue(target.AppliesTo("3111222333444555666777"));
        }

        [Test]
        public void TestAppliesToNull()
        {
            Assert.IsFalse(target.AppliesTo(null));
        }

        [Test]
        public void TestAppliesToEmptyString()
        {
            Assert.IsFalse(target.AppliesTo(string.Empty));
        }

        [Test]
        public void TestIsValidCorrectLength()
        {
            Assert.IsTrue(target.IsValid("3111222333444555", validExpiryDate));
        }

        [Test]
        public void TestIsValidIncorrectLength()
        {
            Assert.IsFalse(target.IsValid("311122233344455", validExpiryDate));
            Assert.IsFalse(target.IsValid("3", validExpiryDate));
            Assert.IsFalse(target.IsValid("31112223334445556", validExpiryDate));
            Assert.IsFalse(target.IsValid("311122233344455566", validExpiryDate));
        }

        [Test]
        public void TestIsValidNull()
        {
            Assert.IsFalse(target.IsValid(null, validExpiryDate));
        }

        [Test]
        public void TestIsValidEmptyString()
        {
            Assert.IsFalse(target.IsValid(string.Empty, validExpiryDate));
        }
    }
}
