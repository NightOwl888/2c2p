﻿using NUnit.Framework;
using System;

namespace _2C2P.Business.CreditCardValidation
{
    [TestFixture]
    public class MasterCardCreditCardValidatorStrategyTest
    {
        private ICreditCardValidatorStrategy target;
        private DateTime validExpiryDate;
        private DateTime invalidExpiryDate;

        [SetUp]
        public void SetUp()
        {
            target = new MasterCardCreditCardValidatorStrategy();
            validExpiryDate = new DateTime(2029, 11, 1);
            invalidExpiryDate = new DateTime(2028, 11, 1);
        }

        [TearDown]
        public void TearDown()
        {
            target = null;
            validExpiryDate = new DateTime();
            invalidExpiryDate = new DateTime();
        }

        [Test]
        public void TestCardTypeReturnsMaster()
        {
            Assert.AreEqual("Master", target.CardType);
        }

        [Test]
        public void TestAppliesToBeginsWith5()
        {
            Assert.IsTrue(target.AppliesTo("5111222333444555"));
        }

        [Test]
        public void TestAppliesToBeginsWith5AndLengthTooShort()
        {
            // IsValid tests for the correct length. Here we are
            // only concerned that it is using the correct validator.
            Assert.IsTrue(target.AppliesTo("511122233344455"));
            Assert.IsTrue(target.AppliesTo("5111222"));
            Assert.IsTrue(target.AppliesTo("5"));
        }

        [Test]
        public void TestAppliesToBeginsWith3AndLengthTooLong()
        {
            // IsValid tests for the correct length. Here we are
            // only concerned that it is using the correct validator.
            Assert.IsTrue(target.AppliesTo("51112223334445556"));
            Assert.IsTrue(target.AppliesTo("5111222333444555666"));
            Assert.IsTrue(target.AppliesTo("5111222333444555666777"));
        }

        [Test]
        public void TestAppliesToNull()
        {
            Assert.IsFalse(target.AppliesTo(null));
        }

        [Test]
        public void TestAppliesToEmptyString()
        {
            Assert.IsFalse(target.AppliesTo(string.Empty));
        }

        [Test]
        public void TestIsValidCorrectLengthAndLeapYearExpiryDate()
        {
            Assert.IsTrue(target.IsValid("5111222333444555", validExpiryDate));
        }

        [Test]
        public void TestIsValidIncorrectLength()
        {
            Assert.IsFalse(target.IsValid("511122233344455", validExpiryDate));
            Assert.IsFalse(target.IsValid("5", validExpiryDate));
            Assert.IsFalse(target.IsValid("51112223334445556", validExpiryDate));
            Assert.IsFalse(target.IsValid("511122233344455566", validExpiryDate));
        }

        [Test]
        public void TestIsValidNonLeapYearExpiryDate()
        {
            Assert.IsFalse(target.IsValid("5111222333444555", invalidExpiryDate));
        }

        [Test]
        public void TestIsValidNull()
        {
            Assert.IsFalse(target.IsValid(null, validExpiryDate));
        }

        [Test]
        public void TestIsValidEmptyString()
        {
            Assert.IsFalse(target.IsValid(string.Empty, validExpiryDate));
        }
    }
}
