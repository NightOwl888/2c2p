﻿using NUnit.Framework;
using System;

namespace _2C2P.Business.CreditCardValidation
{
    [TestFixture]
    public class VisaCreditCardValidatorStrategyTest
    {
        private ICreditCardValidatorStrategy target;
        private DateTime validExpiryDate;
        private DateTime invalidExpiryDate;

        [SetUp]
        public void SetUp()
        {
            target = new VisaCreditCardValidatorStrategy();
            validExpiryDate = new DateTime(2020, 11, 1);
            invalidExpiryDate = new DateTime(2021, 11, 1);
        }

        [TearDown]
        public void TearDown()
        {
            target = null;
            validExpiryDate = new DateTime();
            invalidExpiryDate = new DateTime();
        }

        [Test]
        public void TestCardTypeReturnsVisa()
        {
            Assert.AreEqual("Visa", target.CardType);
        }

        [Test]
        public void TestAppliesToBeginsWith4()
        {
            Assert.IsTrue(target.AppliesTo("4111222333444555"));
        }

        [Test]
        public void TestAppliesToBeginsWith4AndLengthTooShort()
        {
            // IsValid tests for the correct length. Here we are
            // only concerned that it is using the correct validator.
            Assert.IsTrue(target.AppliesTo("411122233344455"));
            Assert.IsTrue(target.AppliesTo("4111222"));
            Assert.IsTrue(target.AppliesTo("4"));
        }

        [Test]
        public void TestAppliesToBeginsWith4AndLengthTooLong()
        {
            // IsValid tests for the correct length. Here we are
            // only concerned that it is using the correct validator.
            Assert.IsTrue(target.AppliesTo("41112223334445556"));
            Assert.IsTrue(target.AppliesTo("4111222333444555666"));
            Assert.IsTrue(target.AppliesTo("4111222333444555666777"));
        }

        [Test]
        public void TestAppliesToNull()
        {
            Assert.IsFalse(target.AppliesTo(null));
        }

        [Test]
        public void TestAppliesToEmptyString()
        {
            Assert.IsFalse(target.AppliesTo(string.Empty));
        }

        [Test]
        public void TestIsValidCorrectLengthAndPrimeNumberYear()
        {
            Assert.IsTrue(target.IsValid("4111222333444555", validExpiryDate));
        }

        [Test]
        public void TestIsValidIncorrectLength()
        {
            Assert.IsFalse(target.IsValid("411122233344455", validExpiryDate));
            Assert.IsFalse(target.IsValid("4", validExpiryDate));
            Assert.IsFalse(target.IsValid("41112223334445556", validExpiryDate));
            Assert.IsFalse(target.IsValid("411122233344455566", validExpiryDate));
        }

        [Test]
        public void TestIsValidNonPrimeNumberYear()
        {
            Assert.IsFalse(target.IsValid("4111222333444555", invalidExpiryDate));
        }

        [Test]
        public void TestIsValidNull()
        {
            Assert.IsFalse(target.IsValid(null, validExpiryDate));
        }

        [Test]
        public void TestIsValidEmptyString()
        {
            Assert.IsFalse(target.IsValid(string.Empty, validExpiryDate));
        }
    }
}
