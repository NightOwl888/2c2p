﻿using Moq;
using NUnit.Framework;
using System;

namespace _2C2P.Business.CreditCardValidation
{
    [TestFixture]
    public class CreditCardValidatorTest
    {
        private ICreditCardValidator target;
        private DateTime fakeDate = new DateTime(2020, 12, 31, 23, 59, 59, 999); // Set to a leap year for testing
        private Mock<ISystemClock> systemClock;
        private bool creditCardExists = true;
        private Mock<ICreditCardExistenceChecker> creditCardExistenceChecker;

        [SetUp]
        public void SetUp()
        {
            systemClock = new Mock<ISystemClock>();
            systemClock.Setup(m => m.GetCurrentTime()).Returns(() => fakeDate);

            creditCardExistenceChecker = new Mock<ICreditCardExistenceChecker>();
            creditCardExistenceChecker.Setup(check => check.CreditCardExists(It.IsAny<string>())).Returns(() => creditCardExists);

            target = new CreditCardValidator(
                new ExpiryDateParser(),
                systemClock.Object,
                creditCardExistenceChecker.Object,
                new ICreditCardValidatorStrategy[] 
                {
                    new VisaCreditCardValidatorStrategy(),
                    new MasterCardCreditCardValidatorStrategy(),
                    new AmexCreditCardValidatorStrategy(),
                    new JCBCreditCardValidatorStrategy()
                }
            );
        }

        [TearDown]
        public void TearDown()
        {
            target = null;
            systemClock = null;
            creditCardExistenceChecker = null;
        }

        [Test]
        public void TestValidateCardType()
        {
            // Visa
            AssertCreditCardTypeEqual("4000000000000005", "022024", "Visa"); // Valid length
            AssertCreditCardTypeEqual("400000000000000", "022024", "Visa"); // Invalid length
            AssertCreditCardTypeEqual("40000000000000000", "022024", "Visa"); // Invalid length
            // MasterCard
            AssertCreditCardTypeEqual("5000000000000006", "022029", "Master"); // Valid length
            AssertCreditCardTypeEqual("500000000000000", "022029", "Master"); // Invalid length
            AssertCreditCardTypeEqual("50000000000000067", "022029", "Master"); // Invalid length
            // Amex
            AssertCreditCardTypeEqual("300000000000005", "022024", "Amex"); // Valid length
            AssertCreditCardTypeEqual("30000000000000", "022024", "Amex"); // Invalid length
            AssertCreditCardTypeEqual("3000000000", "022024", "Amex"); // Invalid length
            // JCB
            AssertCreditCardTypeEqual("3000000000000006", "022029", "JCB"); // Valid length
            AssertCreditCardTypeEqual("300000000000000666", "022029", "JCB"); // Invalid length
            AssertCreditCardTypeEqual("30000000000000067", "022029", "JCB"); // Invalid length
            // Unknown
            for (int i = 0; i < 10; i++)
            {
                if (i < 3 || i > 5)
                    AssertCreditCardTypeEqual(i.ToString() + "000000000000000", "022029", "Unknown");
            }
        }

        #region Visa

        [Test]
        public void TestValidateValidVisa()
        {
            AssertCreditCardValid("4000000000000000", "022024", "Visa", Domain.CreditCardValidationResult.Valid);
            AssertCreditCardValid("4000000000000000", "022028", "Visa", Domain.CreditCardValidationResult.Valid);
            AssertCreditCardValid("4000000000000000", "022032", "Visa", Domain.CreditCardValidationResult.Valid);
        }

        [Test]
        public void TestValidateInvalidVisaNumber()
        {
            string cardNumber;
            string expiryDate = "022024";
            for (int i = 0; i < 10; i++)
            {
                if (i != 4)
                {
                    cardNumber = i.ToString() + "000000000000000";

                    if (i < 3 || i > 5)
                        AssertCreditCardNumberInvalid(cardNumber, expiryDate);
                    AssertCreditCardTypeNotEqual(cardNumber, expiryDate, "Visa");
                }
            }
        }

        [Test]
        public void TestValidateInvalidVisaLeapYear()
        {
            for (int i = 2025; i < 2325; i++)
            {
                if (!DateTime.IsLeapYear(i))
                    AssertCreditCardNumberInvalid("4000000000000000", "022019"); // Not a leap year, should be invalid
            }
        }

        [Test]
        public void TestValidateInvalidVisaLength()
        {
            AssertCreditCardNumberInvalid("400000000000000", "022024"); // only 15 digits
        }

        [Test]
        public void TestValidateInvalidVisaDoesNotExist()
        {
            this.creditCardExists = false;
            AssertCreditCardNumberDoesNotExist("4000000000000000", "022024");
            this.creditCardExists = true;
        }

        #endregion

        #region MasterCard

        [Test]
        public void TestValidateValidMasterCard()
        {
            // Years are prime numbers
            AssertCreditCardValid("5000000000000000", "022027", "Master", Domain.CreditCardValidationResult.Valid);
            AssertCreditCardValid("5000000000000000", "022029", "Master", Domain.CreditCardValidationResult.Valid);
            AssertCreditCardValid("5000000000000000", "022039", "Master", Domain.CreditCardValidationResult.Valid);
        }

        [Test]
        public void TestValidateInvalidMasterCardNumber()
        {
            string cardNumber;
            string expiryDate = "022027";
            for (int i = 0; i < 10; i++)
            {
                if (i != 5)
                {
                    cardNumber = i.ToString() + "000000000000000";

                    if (i < 3 || i > 5)
                        AssertCreditCardNumberInvalid(cardNumber, expiryDate);
                    AssertCreditCardTypeNotEqual(cardNumber, expiryDate, "Master");
                }
            }
        }

        [Test]
        public void TestValidateInvalidMasterCardPrimeNumber()
        {
            for (int i = 2025; i < 2325; i++)
            {
                if (!i.IsPrime())
                    AssertCreditCardNumberInvalid("5000000000000000", "02" + i.ToString()); // Not a prime, should be invalid
            }
        }

        [Test]
        public void TestValidateInvalidMasterCardLength()
        {
            AssertCreditCardNumberInvalid("500000000000000", "022029"); // only 15 digits
        }

        [Test]
        public void TestValidateInvalidMasterCardDoesNotExist()
        {
            this.creditCardExists = false;
            AssertCreditCardNumberDoesNotExist("5000000000000000", "022029");
            this.creditCardExists = true;
        }

        #endregion

        #region AmericanExpress

        [Test]
        public void TestValidateValidAmex()
        {
            AssertCreditCardValid("300000000000000", "022027", "Amex", Domain.CreditCardValidationResult.Valid);
        }

        [Test]
        public void TestValidateInvalidAmexCardNumber()
        {
            string cardNumber;
            string expiryDate = "022027";
            for (int i = 0; i < 10; i++)
            {
                if (i != 3)
                {
                    cardNumber = i.ToString() + "00000000000000";

                    if (i < 3 || i > 5)
                        AssertCreditCardNumberInvalid(cardNumber, expiryDate);
                    AssertCreditCardTypeNotEqual(cardNumber, expiryDate, "Amex");
                }
            }
        }

        [Test]
        public void TestValidateInvalidAmexLength()
        {
            AssertCreditCardNumberInvalid("30000000000000000", "022029"); // 17 digits (16 would be a valid JCB card)
            AssertCreditCardNumberInvalid("30000000000000", "022029"); // 14 digits
        }

        [Test]
        public void TestValidateInvalidAmexDoesNotExist()
        {
            this.creditCardExists = false;
            AssertCreditCardNumberDoesNotExist("300000000000000", "022029");
            this.creditCardExists = true;
        }

        #endregion

        #region JCB

        [Test]
        public void TestValidateValidJCB()
        {
            AssertCreditCardValid("3000000000000000", "022027", "JCB", Domain.CreditCardValidationResult.Valid);
        }

        [Test]
        public void TestValidateInvalidJCBCardNumber()
        {
            string cardNumber;
            string expiryDate = "022027";
            for (int i = 0; i < 10; i++)
            {
                if (i != 3)
                {
                    cardNumber = i.ToString() + "000000000000000";

                    if (i < 3 || i > 5)
                        AssertCreditCardNumberInvalid(cardNumber, expiryDate);
                    AssertCreditCardTypeNotEqual(cardNumber, expiryDate, "JCB");
                }
            }
        }

        [Test]
        public void TestValidateInvalidJCBLength()
        {
            AssertCreditCardNumberInvalid("30000000000000000", "022029"); // 17 digits 
            AssertCreditCardNumberInvalid("30000000000000", "022029"); // 14 digits (15 would be a valid Amex card)
        }

        [Test]
        public void TestValidateInvalidJCBDoesNotExist()
        {
            this.creditCardExists = false;
            AssertCreditCardNumberDoesNotExist("3000000000000000", "022029");
            this.creditCardExists = true;
        }

        #endregion


        [Test]
        public void TestValidateExpiryDate()
        {
            fakeDate = new DateTime(2020, 12, 31, 23, 59, 59, 999);
            var cardNumber = "4000000000000000";

            // Anything before or equal to 12/2020 is invalid
            AssertCreditCardNumberInvalid(cardNumber, "022016");
            AssertCreditCardNumberInvalid(cardNumber, "121956");

            AssertCreditCardNumberValid(cardNumber, "122020");
            AssertCreditCardNumberValid(cardNumber, "042056");

            // Anything before or equal to 01/2016 is invalid
            fakeDate = new DateTime(2016, 1, 31, 23, 59, 59, 999);

            AssertCreditCardNumberValid(cardNumber, "022016");
            AssertCreditCardNumberInvalid(cardNumber, "121956");

            AssertCreditCardNumberValid(cardNumber, "122020");
            AssertCreditCardNumberValid(cardNumber, "042056");
        }

        private void AssertCreditCardValid(string cardNumber, string expiryDate, 
            string expectedCardType, Domain.CreditCardValidationResult expectedResult)
        {
            var result = target.Validate(new Domain.Card { CardNumber = cardNumber, ExpiryDate = expiryDate });
            Assert.AreEqual(expectedCardType, result.CardType);
            Assert.AreEqual(expectedResult, result.Result);
        }

        private void AssertCreditCardNumberValid(string cardNumber, string expiryDate)
        {
            var result = target.Validate(new Domain.Card { CardNumber = cardNumber, ExpiryDate = expiryDate });
            Assert.AreEqual(Domain.CreditCardValidationResult.Valid, result.Result);
        }

        private void AssertCreditCardNumberInvalid(string cardNumber, string expiryDate)
        {
            var result = target.Validate(new Domain.Card { CardNumber = cardNumber, ExpiryDate = expiryDate });
            Assert.AreEqual(Domain.CreditCardValidationResult.Invalid, result.Result);
        }

        private void AssertCreditCardNumberDoesNotExist(string cardNumber, string expiryDate)
        {
            var result = target.Validate(new Domain.Card { CardNumber = cardNumber, ExpiryDate = expiryDate });
            Assert.AreEqual(Domain.CreditCardValidationResult.DoesNotExist, result.Result);
        }

        private void AssertCreditCardTypeNotEqual(string cardNumber, string expiryDate, string expectedCardType)
        {
            var result = target.Validate(new Domain.Card { CardNumber = cardNumber, ExpiryDate = expiryDate });
            Assert.AreNotEqual(expectedCardType, result.CardType);
        }

        private void AssertCreditCardTypeEqual(string cardNumber, string expiryDate, string expectedCardType)
        {
            var result = target.Validate(new Domain.Card { CardNumber = cardNumber, ExpiryDate = expiryDate });
            Assert.AreEqual(expectedCardType, result.CardType);
        }
    }
}
