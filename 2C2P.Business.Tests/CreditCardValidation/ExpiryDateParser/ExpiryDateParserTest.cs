﻿using NUnit.Framework;
using System;

namespace _2C2P.Business.CreditCardValidation
{
    [TestFixture]
    public class ExpiryDateParserTest
    {
        private IExpiryDateParser target;

        [SetUp]
        public void SetUp()
        {
            target = new ExpiryDateParser();
        }

        [TearDown]
        public void TearDown()
        {
            target = null;
        }

        [Test]
        public void TestParseValid()
        {
            DateTime result;

            Assert.IsTrue(target.TryParse("112020", out result));
            AssertExpiryDateEquals(result, 11, 2020);

            Assert.IsTrue(target.TryParse("011979", out result));
            AssertExpiryDateEquals(result, 1, 1979);

            Assert.IsTrue(target.TryParse("122030", out result));
            AssertExpiryDateEquals(result, 12, 2030);
        }

        private void AssertExpiryDateEquals(DateTime expiryDate, int month, int year)
        {
            var day = 1; // first day of month
            Assert.That(expiryDate, Is.EqualTo(new DateTime(year, month, day).Date.AddMonths(1).AddMilliseconds(-1)));
        }

        [Test]
        public void TestParseInvalidFormat()
        {
            DateTime result;

            Assert.IsFalse(target.TryParse("139999", out result)); // Invalid month
            Assert.IsFalse(target.TryParse("120000", out result)); // Invalid year
            Assert.IsFalse(target.TryParse("11202", out result)); // Invalid length
            Assert.IsFalse(target.TryParse("1120202", out result)); // Invalid length
            Assert.IsFalse(target.TryParse("1a2020", out result)); // Non-digit
        }

        [Test]
        public void TestParseNull()
        {
            DateTime result;

            Assert.IsFalse(target.TryParse(null, out result));
        }

        [Test]
        public void TestParseEmptyString()
        {
            DateTime result;

            Assert.IsFalse(target.TryParse("", out result));
        }

        [Test]
        public void TestParseWhitespace()
        {
            DateTime result;

            Assert.IsFalse(target.TryParse(" ", out result));
            Assert.IsFalse(target.TryParse("    ", out result));
            Assert.IsFalse(target.TryParse("      ", out result));
        }
    }
}
