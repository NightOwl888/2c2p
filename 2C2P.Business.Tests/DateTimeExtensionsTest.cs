﻿using NUnit.Framework;
using System;

namespace _2C2P.Business
{
    [TestFixture]
    public class DateTimeExtensionsTest
    {
        [Test]
        public void TestToLastDayOfMonthAtMidnight()
        {
            Assert.AreEqual(new DateTime(2020, 1, 31, 23, 59, 59, 999), new DateTime(2020, 1, 1, 23, 59, 59, 999).ToLastDayOfMonthAtMidnight());
            Assert.AreEqual(new DateTime(2020, 2, 29, 23, 59, 59, 999), new DateTime(2020, 2, 20, 23, 59, 59, 999).ToLastDayOfMonthAtMidnight());
        }
    }
}
