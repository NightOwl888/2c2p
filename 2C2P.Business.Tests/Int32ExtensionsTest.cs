﻿using NUnit.Framework;

namespace _2C2P.Business
{
    [TestFixture]
    public class Int32ExtensionsTest
    {
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(5)]
        [TestCase(7)]
        [TestCase(11)]
        [TestCase(2027)]
        [TestCase(2029)]
        [TestCase(2039)]
        public void TestIsPrimeValidNumbers(int prime)
        {
            Assert.IsTrue(prime.IsPrime());
        }

        [TestCase(1)]
        [TestCase(4)]
        [TestCase(6)]
        [TestCase(8)]
        [TestCase(9)]
        [TestCase(2025)]
        [TestCase(2030)]
        [TestCase(2037)]
        public void TestIsPrimeInvalidNumbers(int prime)
        {
            Assert.IsFalse(prime.IsPrime());
        }
    }
}
