﻿using NUnit.Framework;

namespace _2C2P.Business
{
    [TestFixture]
    public class StringExtensionsTest
    {
        [Test]
        public void TestContainsOnlyDigits()
        {
            Assert.IsTrue("0123456789".ContainsOnlyDigits());
        }

        [TestCase("0123456789a")]
        [TestCase("abcde")]
        [TestCase("ภาษาไทย")]
        [TestCase("๑๒๓๔")]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void TestContainsOnlyDigitsInvalid(string target)
        {
            Assert.IsFalse(target.ContainsOnlyDigits());
        }
    }
}
