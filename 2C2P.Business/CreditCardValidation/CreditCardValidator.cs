﻿using _2C2P.Business.CreditCardValidation.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// Validates a credit card number to determine what type of card it is and whether it is valid.
    /// </summary>
    public class CreditCardValidator : ICreditCardValidator
    {
        private readonly IExpiryDateParser expiryDateParser;
        private readonly ISystemClock systemClock;
        private readonly ICreditCardExistenceChecker creditCardExistenceChecker;
        private readonly IEnumerable<ICreditCardValidatorStrategy> creditCardValidatorStrategies;

        public CreditCardValidator(
            IExpiryDateParser expiryDateParser,
            ISystemClock systemClock,
            ICreditCardExistenceChecker creditCardExistenceChecker,
            IEnumerable<ICreditCardValidatorStrategy> creditCardValidatorStrategies)
        {
            this.expiryDateParser = expiryDateParser
                ?? throw new ArgumentNullException(nameof(expiryDateParser));
            this.systemClock = systemClock 
                ?? throw new ArgumentNullException(nameof(systemClock));
            this.creditCardExistenceChecker = creditCardExistenceChecker
                ?? throw new ArgumentNullException(nameof(creditCardExistenceChecker));
            this.creditCardValidatorStrategies = creditCardValidatorStrategies 
                ?? throw new ArgumentNullException(nameof(creditCardValidatorStrategies));
        }

        public ValidateResult Validate(Card card)
        {
            CreditCardValidationResult result = CreditCardValidationResult.Valid;
            // Get the correct validator strategy according to card type
            var validator = creditCardValidatorStrategies.FirstOrDefault(x => x.AppliesTo(card.CardNumber));

            if (!expiryDateParser.TryParse(card.ExpiryDate, out DateTime expiryDate))
            {
                result = CreditCardValidationResult.Invalid;
            }
            else
            {
                // Verify the card is still valid right now
                // TODO: This logic may need to be adjusted for time zone
                bool cardExpired = (systemClock.GetCurrentTime() > expiryDate);

                if (cardExpired || validator == null || !validator.IsValid(card.CardNumber, expiryDate))
                    result = CreditCardValidationResult.Invalid;
                else if (!creditCardExistenceChecker.CreditCardExists(card.CardNumber))
                    result = CreditCardValidationResult.DoesNotExist;
            }

            return new ValidateResult
            {
                CardType = (validator == null) ? "Unknown" : validator.CardType,
                Result = result
            };
        }
    }
}
