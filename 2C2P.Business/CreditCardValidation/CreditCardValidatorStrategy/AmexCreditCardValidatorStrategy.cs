﻿using System;

namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// Credit card validator strategy for Amex card type
    /// </summary>
    public class AmexCreditCardValidatorStrategy : ICreditCardValidatorStrategy
    {
        private const int ValidCardLength = 15;

        public string CardType => "Amex";

        public bool AppliesTo(string cardNumber)
        {
            return cardNumber != null && cardNumber.StartsWith("3")
                // The "rest" case is "Invalid" card (rather than unknown),
                // so we will make anything with less than 16 digits Amex
                // and lean on the IsValid method to set the status.
                && cardNumber.Length <= ValidCardLength;
        }

        public bool IsValid(string cardNumber, DateTime expiryDate)
        {
            return cardNumber != null && cardNumber.Length == ValidCardLength;
        }
    }
}
