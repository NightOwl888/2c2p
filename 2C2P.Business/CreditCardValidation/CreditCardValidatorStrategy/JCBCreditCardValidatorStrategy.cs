﻿using System;

namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// Credit card validator strategy for JCB card type
    /// </summary>
    public class JCBCreditCardValidatorStrategy : ICreditCardValidatorStrategy
    {
        private const int ValidCardLength = 16;

        public string CardType => "JCB";

        public bool AppliesTo(string cardNumber)
        {
            return cardNumber != null && cardNumber.StartsWith("3")
                // The "rest" case is "Invalid" card (rather than unknown),
                // so we will make anything with greater than 16 digits JCB
                // and lean on the IsValid method to set the status.
                && cardNumber.Length >= ValidCardLength;
        }

        public bool IsValid(string cardNumber, DateTime expiryDate)
        {
            return cardNumber != null && cardNumber.Length == ValidCardLength; // All JCB cards are valid (if they are the right length)
        }
    }
}
