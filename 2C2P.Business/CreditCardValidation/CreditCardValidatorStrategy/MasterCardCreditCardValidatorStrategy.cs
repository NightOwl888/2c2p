﻿using System;

namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// Credit card validator strategy for Visa card type
    /// </summary>
    public class MasterCardCreditCardValidatorStrategy : ICreditCardValidatorStrategy
    {
        private const int ValidCardLength = 16;

        public string CardType => "Master";

        public bool AppliesTo(string cardNumber)
        {
            return cardNumber != null && cardNumber.StartsWith("5");
        }

        public bool IsValid(string cardNumber, DateTime expiryDate)
        {
            return expiryDate.Year.IsPrime()
                // The "rest" case is "Invalid" card (rather than unknown)
                && cardNumber != null && cardNumber.Length == ValidCardLength;
        }
    }
}
