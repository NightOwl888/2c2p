﻿using System;

namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// Credit card validator strategy for Visa card type
    /// </summary>
    public class VisaCreditCardValidatorStrategy : ICreditCardValidatorStrategy
    {
        private const int ValidCardLength = 16;

        public string CardType => "Visa";

        public bool AppliesTo(string cardNumber)
        {
            return cardNumber != null && cardNumber.StartsWith("4");
        }

        public bool IsValid(string cardNumber, DateTime expiryDate)
        {
            return DateTime.IsLeapYear(expiryDate.Year)
                // The "rest" case is "Invalid" card (rather than unknown)
                && cardNumber != null && cardNumber.Length == ValidCardLength;
        }
    }
}
