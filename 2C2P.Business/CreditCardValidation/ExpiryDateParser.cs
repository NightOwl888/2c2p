﻿using System;

namespace _2C2P.Business.CreditCardValidation
{
    /// <summary>
    /// A parser to convert a string MMYYYY date into a <see cref="DateTime"/> structure.
    /// </summary>
    public class ExpiryDateParser : IExpiryDateParser
    {
        public bool TryParse(string expiryDate, out DateTime result)
        {
            if (expiryDate == null || expiryDate.Length != 6 || !expiryDate.ContainsOnlyDigits())
            {
                result = DateTime.MinValue;
                return false;
            }

            int month = int.Parse(expiryDate.Substring(0, 2));
            int year = int.Parse(expiryDate.Substring(2, 4));
            int day = 1;

            // Return false if the numbers passed don't represent a valid DateTime
            if (year < 1 || year > 9999 || month < 1 | month > 12)
            {
                result = DateTime.MinValue;
                return false;
            }

            // Change to last day of the month at the last millisecond
            result = new DateTime(year, month, day).ToLastDayOfMonthAtMidnight();
            return true;
        }
    }
}
