﻿using System;

namespace _2C2P.Business
{
    /// <summary>
    /// Extension methods for <see cref="DateTime"/>.
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Converts the <paramref name="input"/> into a date that is 1 millisecond before
        /// the end of the month (midnight) of <paramref name="input"/>. This effectively
        /// ignores the day and time component when comparing for a date of expiry for the month and year.
        /// </summary>
        /// <param name="input">This <see cref="DateTime"/>.</param>
        /// <returns>A <see cref="DateTime"/> set to the last millisecond of the month of <paramref name="input"/>.</returns>
        public static DateTime ToLastDayOfMonthAtMidnight(this DateTime input)
        {
            return input.Date               // Remove the time component
                .AddDays(-input.Day + 1)    // Set to first day of month
                .AddMonths(1)               // Get the beginning of next month
                .AddMilliseconds(-1);       // Set to 1 millisecond before the beginning of next month
        }
    }
}
