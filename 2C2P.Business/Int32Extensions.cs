﻿using System.Linq;

namespace _2C2P.Business
{
    /// <summary>
    /// Extension methods
    /// </summary>
    public static class Int32Extensions
    {
        /// <summary>
        /// Determines whether the current number is a prime number.
        /// </summary>
        /// <param name="input">This integer.</param>
        /// <returns><b>True</b> if the <paramref name="input"/> is a prime number, otherwise returns <b>false</b></returns>.
        public static bool IsPrime(this int input)
        {
            if (input > 1)
            {
                return Enumerable.Range(1, input).Where(x => input % x == 0)
                                 .SequenceEqual(new[] { 1, input });
            }

            return false;
        }
    }
}
