﻿using System.Linq;

namespace _2C2P.Business
{
    /// <summary>
    /// Extension methods for <see cref="string"/>.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Determines if the current string contains only digits 0 through 9.
        /// </summary>
        /// <param name="input"></param>
        /// <returns><b>True</b> if the current string only contains digits 0 through 9; otherwise <b>false</b>.</returns>
        public static bool ContainsOnlyDigits(this string input)
        {
            return !string.IsNullOrWhiteSpace(input)
                && input.All(c => c >= '0' && c <= '9');
        }
    }
}
