﻿using System;

namespace _2C2P.Business
{
    /// <summary>
    /// Service that provides the current time.
    /// </summary>
    public class SystemClock : ISystemClock
    {
        public DateTime GetCurrentTime()
        {
            return DateTime.Now;
        }
    }
}
