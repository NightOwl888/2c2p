﻿using _2C2P.Data.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace _2C2P.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("DefaultConnection")
        {
#if DEBUG
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
#endif
            
        }

        public IDbSet<CreditCard> CreditCards { get; set; }

        public DbConnection Connection
        {
            get { return this.Database.Connection; }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            // Make table names singular.
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<CreditCard>()
                .Property(c => c.CardNumber)
                .HasMaxLength(20);

            modelBuilder.Entity<CreditCard>()
                .Property(c => c.CardNumber)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<CreditCard>()
                .HasKey(c => c.CardNumber);
        }
    }
}
