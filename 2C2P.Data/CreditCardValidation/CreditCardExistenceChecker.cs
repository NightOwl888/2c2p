﻿using _2C2P.Business.CreditCardValidation;
using System;
using System.Data;
using System.Data.Common;

namespace _2C2P.Data.CreditCardValidation
{
    /// <summary>
    /// Checks for the existence of a credit card number in a database
    /// </summary>
    public class CreditCardExistenceChecker : ICreditCardExistenceChecker
    {
        private readonly ApplicationDbContext dbContext;
        private readonly DbCommand dbCommand;

        public CreditCardExistenceChecker(ApplicationDbContext dbContext, DbCommand dbCommand)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            this.dbCommand = dbCommand ?? throw new ArgumentNullException(nameof(dbCommand));
        }

        public bool CreditCardExists(string cardNumber)
        {
            this.dbCommand.CommandText = "CreditCardExists";
            this.dbCommand.CommandType = CommandType.StoredProcedure;

            var cardNumberParam = this.dbCommand.CreateParameter();
            cardNumberParam.ParameterName = "@CardNumber";
            cardNumberParam.Value = cardNumber;
            this.dbCommand.Parameters.Add(cardNumberParam);

            var returnValueParam = this.dbCommand.CreateParameter();
            returnValueParam.Direction = ParameterDirection.ReturnValue;
            this.dbCommand.Parameters.Add(returnValueParam);

            this.dbCommand.Connection = this.dbContext.Connection;
            int result = -1;

            this.dbCommand.Connection.Open();
            try
            {
                var i = this.dbCommand.ExecuteNonQuery();
                result = Convert.ToInt32(returnValueParam.Value);
            }
            finally
            {
                this.dbCommand.Connection.Close();
            }

            return result == 1;
        }
    }
}
