namespace _2C2P.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CreditCard",
                c => new
                    {
                        CardNumber = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.CardNumber);

            // Create a stored procedure for use in testing existence of the card number.
            CreateStoredProcedure(
                "CreditCardExists",
                p => new
                {
                    CardNumber = p.String(maxLength: 20)
                },
                @"RETURN CASE WHEN EXISTS (SELECT CardNumber " +
                @"           FROM dbo.CreditCard " +
                @"           WHERE CardNumber = @CardNumber) " +
                @"       THEN CAST (1 AS INT) " +
                @"       ELSE CAST (0 AS INT) END");
        }

        public override void Down()
        {
            DropStoredProcedure("CreditCardExists");
            DropTable("dbo.CreditCard");
        }
    }
}
