﻿using System.Data.Entity.Migrations;

namespace _2C2P.Data.Seeding
{
    internal class DemoSeeder
    { 
        public void Seed(ApplicationDbContext context)
        {
            this.SeedCreditCard(context);
        }

        private void SeedCreditCard(ApplicationDbContext context)
        {
            context.CreditCards.AddOrUpdate(p => p.CardNumber, SampleCreditCards.GetCreditCards());
            context.SaveChanges();
        }
    }
}
